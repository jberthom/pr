import cv2
import numpy as np
from picamera import PiCamera
from time import sleep



def photomask() {

    #capture photo
    camera = PiCamera()
    camera.start_preview()
    sleep(5)
    camera.capture('/home/pi/color_rec.jpg')
    camera.stop_preview()
    camera.close()
    
    #reco

    image = cv2.imread("/home/pi/color_rec.jpg")

    # Convert BGR to HSV

    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    # define color strenght parameters in HSV

    weaker = np.array([160, 100, 100])
    stronger = np.array([179, 255, 255])


    # Threshold the HSV image to obtain input color

    mask = cv2.inRange(hsv, weaker, stronger)

    cv2.imwrite('result.jpg',mask)

    cv2.imshow('Image',image)
    cv2.imshow('Result',mask)

    cv2.waitKey(0)
    cv2.destroyAllWindows()

}

