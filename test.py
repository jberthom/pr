import sys
from PIL import Image
import cv2
import numpy as np

ImageFile = '/home/pi/result.jpg'

try:
	img = cv2.imread(ImageFile)
	hsv = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
except IOError:
  print('Erreur sur ouverture du fichier ' + ImageFile)
  sys.exit(1)
# affichage des caractéristiques de l'image
# print img.format,img.size, img.mode
# affichage de l'image
# img.show()
# fermeture du fichier image

imgF = np.array(hsv, copy=True)

white_px = np.asarray([255, 255, 255])
black_px = np.asarray([0, 0, 0])

largeur_max = 0
hauteur_max = 0

(row, col,_) = img.shape

#for r in range(row):
     #for c in range(col):
         #px = hsv[r][c]
         ##if all(px == white_px):
         #if all(px != black_px):
             #imgF[r][c] = all(black_px)

for r in range(row):
   for c in range(col):
      px = hsv[r][c]
      if all(px !=black_px):
	 imgF[r][c] = all(black_px)
