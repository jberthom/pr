import sys
from PIL import Image
import cv2
import numpy as np

ImageFile = '/home/pi/result.jpg'

try:
    img = cv2.imread(ImageFile)
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
except IOError:
  print('Erreur sur ouverture du fichier ' + ImageFile)
  sys.exit(1)
# affichage des caractéristiques de l'image
#print img.format,img.size, img.mode
# affichage de l'image
# img.show()
# fermeture du fichier image

imgF = np.array(hsv, copy=True)

white_px = np.asarray([255, 255, 255])
black_px = np.asarray([0, 0, 0])

(row, col,_) = img.shape

#for r in range(row):
     #for c in range(col):
         #px = hsv[r][c]
         ##if all(px == white_px):
         #if all(px != black_px):
             #imgF[r][c] = all(black_px)

largeur_max = 0
hauteur_max = 0
c_max = 0
r_max = 0


for r in range(row):
    for c in range(col):
        px = hsv[r][c]
        if all(px != black_px):
            px_col = px
            px_lig = px
            hauteur = -1
            largeur = -1
            coll = c
            lig = r
            # hauteur +
            while all(px_col != black_px):
                hauteur = hauteur+1
                coll = coll+1
                px_col = hsv[lig][coll]
            px_col = px
            coll = c
            # hauteur -
            while all(px_col != black_px):
                hauteur = hauteur+1
                coll = coll-1
                px_col = hsv[lig][coll]
            # largeur +
            px_col = px
            coll = c
            while all(px_lig != black_px):
                largeur = largeur+1
                lig = lig+1
                px_lig = hsv[lig][coll]
            px_lig = px
            lig = r
            # largeur -
            while all(px_lig != black_px):
                largeur = largeur+1
                lig = lig-1
                px_lig = hsv[lig][coll]
            px_lig = px
            lig = r
            if ((largeur + hauteur) > (largeur_max + hauteur_max)):
                largeur_max = largeur
                hauteur_max = hauteur
                c_max = c
                r_max = r

print(c_max)
print(r_max)
for i in range(8):
        imgF[r_max][c_max] = [0, 0, 255]
        r_max = r_max+1


c_max_mm = c_max*0.07856
r_max_mm = r_max*0.104963

print(c_max_mm)
print(r_max_mm)

cv2.imwrite('pixel_detected.jpg',imgF)
cv2.imshow('Result',imgF)


cv2.waitKey(0)
cv2.destroyAllWindows()
